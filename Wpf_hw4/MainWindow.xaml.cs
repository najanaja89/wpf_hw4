﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;


namespace Wpf_hw4
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void TabItem_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            TabItem item = new TabItem();
            item.Header = Browser.Items.Count.ToString();
            Browser.Items.Insert(Browser.SelectedIndex, item);
            WebBrowser web = new WebBrowser();
            Uri uri = new Uri("http://www.google.ru/");
            web.Source = uri;
            item.Content = web;
        }
    }
}
